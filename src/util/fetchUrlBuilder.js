const BaseUrl = 'https://reqres.in';
class UrlBuilder {
    static usersList(page=1){
        return `${BaseUrl}/api/users?page=${page}`
    }
    static user(id){
        return `${BaseUrl}/api/users${id ? '/'+id : ''}`
    }
}

export default UrlBuilder;