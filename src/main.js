import Vue from 'vue'
import VueRouuter from 'vue-router'
import Vuex from 'vuex'
import App from './App.vue'
import router from './routes'
import user from './store_modules/users'
import vuetify from './plugins/vuetify'

Vue.config.productionTip = true
Vue.use(VueRouuter)
Vue.use(Vuex)

const store = new Vuex.Store({
  modules :{
      user
  }
})

new Vue({
  render: h => h(App),
  router,
  vuetify,
  store
}).$mount('#app')
