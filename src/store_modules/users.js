import URLBuilder from '../util/fetchUrlBuilder'
class User {
    constructor(obj){
        this.userData = obj;
        this.isRemoved = false;
    }
    eq(user){
        return this.user.getId() === user.getId()
    }
    getId(){
        return this.userData.id.toString()
    }
    remove(){
        this.isRemoved = true
    }
    removed(){
        return this.isRemoved
    }
    update(obj){
        this.userData = Object.assign(this.userData, obj)
    }
}
export default {
    state : {
        loadedUsers : [],
        loadedPages : [],
        removedLoadedUsers : [],
        usersOnPage : 0,
        totalUsers: 0,
        createdUsers : [],
        usersMap : {}
    },
    getters : {
        usersOnPage(state){
            return state.usersOnPage;
        },
        loadedUsers(state){
            return state.loadedUsers.map(id=>state.usersMap[id])
        },
        totalUsersToLoad(state){
            return state.totalUsers
        },
        allActiveUsers(state){
            return () => Object.keys(state.usersMap)
                .map(key=>state.usersMap[key])
                .filter(user=>!user.removed())
                .concat(state.createdUsers)
        },
        loadedPages(state){
            return state.loadedPages
        },
        removedLoadedUsers(state){
            return state.removedLoadedUsers
        },
        createdUsers(state){
            return state.createdUsers
        },
        totalActiveUsers(state, getters){
            return state.totalUsers - getters.removedLoadedUsers.length + state.createdUsers.length
        }
        
    },
    mutations : {
        pageLoaded(state,page){
            state.loadedPages.push(page);
            state.loadedPages.sort((a,b)=>a-b)
        },
        addLoadedUsers(state, {users, page}){
            if(!state.loadedUsers.length){
                state.loadedUsers = state.loadedUsers.concat(users.map(user=>{
                    this.commit('addUserToMap', user);
                    return user.getId();
                }));
                return;
            }
            let insert_index = state.loadedPages.indexOf(page)*state.usersOnPage;
            state.loadedUsers.splice(insert_index, 0, ...users.map(user=>{
                this.commit('addUserToMap', user);
                return user.getId();
            }))
        },
        setTotalUsers(state, totalUsers){
            state.totalUsers = totalUsers
        },
        setUsersPerPage(state, usersOnPage){
            state.usersOnPage = usersOnPage
        },
        removeUser(state, id){
            if(state.usersMap[id]){
                state.usersMap[id].remove()
                state.removedLoadedUsers = [state.usersMap[id]].concat(state.removedLoadedUsers)
            }else{
                state.createdUsers = state.createdUsers.filter(user => user.getId() != id)
            }
        },
        addUser(state,data){
            state.createdUsers = [data].concat(state.createdUsers)
        },
        addUserToMap(state, user){
            state.usersMap[user.getId()] = user;
        }
    },
    actions : {
        async loadUsers({commit},page=1) {
            let data = await fetch(URLBuilder.usersList(page), {
                method: 'GET'         
            })
            let users = await data.json();
            commit('setUsersPerPage', users.per_page)
            commit('setTotalUsers', users.total)
            commit('pageLoaded', page)
            commit('addLoadedUsers', {
                users : users.data.map(userData => {
                    userData.id = userData.id.toString() 
                    return new User(userData)
                }),
                page
            })
        },
        async getUsers({getters, dispatch}, page=1){
            if(getters.loadedPages.indexOf(page) < 0 && page*getters.usersOnPage <= getters.totalUsersToLoad){
                await dispatch('loadUsers', page)
            }
            let starting_index = 0;
            if(getters.loadedPages.indexOf(page) < 0){
                starting_index = (getters.loadedPages.length + page - Math.ceil(getters.totalUsersToLoad/getters.usersOnPage) - 1)*getters.usersOnPage 
            }else{
                starting_index = getters.loadedPages.indexOf(page)*getters.usersOnPage
            }
            let users = getters.loadedUsers.splice(starting_index, getters.usersOnPage);
            if(page*getters.usersOnPage < getters.totalUsersToLoad 
                && users.length < getters.usersOnPage 
                && getters.loadedPages.indexOf(page + 1) < 0){
                await dispatch('loadUsers', page + 1)
            }
            return getters.allActiveUsers().splice(starting_index, getters.usersOnPage)   
        },
        async getUserById({getters}, id){
            let user = getters.allActiveUsers().find(user => user.getId() == id)
            if(user){
                return user; 
            }
            if(getters.removedLoadedUsers.find(user => user.getId() == id)){
                throw Error('User not found')
            }
            user = await fetch(URLBuilder.user(id), {
                method: 'GET'
            });
            if(user.status !== 200){
                throw Error('User not found')
            }
            let userData = await user.json()
            return new User(userData.data)
        },
        async saveUser({commit}, userData){
            try{
                let method = 'PUT',
                    url = URLBuilder.user(userData.id)
                if(!userData.id){
                    userData.id = (Math.random().toString(36)+'00000000').slice(2, 8);
                    method = 'POST'
                }
                if(!userData.avatar){
                    userData.avatar = 'https://semantic-ui.com/images/wireframe/image.png'
                }
                let responce = await fetch(url, {
                    method,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(Object.keys(userData).reduce(( result, key)=>{
                        if(key !== 'id'){
                            result[key] = userData[key]
                        }
                        return result
                    }, {}))
                }); // as the result we should apply id of the returned data but as returned data is always teh same
                    // we are not going to apply id
                if(!/20(1|0)/.test(responce.status)){
                    throw new Error('An error has occured during user update action');    
                }
                const user = new User(userData);
                if(method === 'POST'){
                    commit('addUser',user)
                }else{
                    commit('addUserToMap', user);
                }
                return userData.id;
            }catch(err){
                console.log(err)
            }
        },
        async removeUser({commit}, id){
            try{
                let result = await fetch(URLBuilder.user(id), {
                    method: 'DELETE'
                });
                if(result.status !== 204){
                    throw new Error('Error occured during removal of the user')
                }
                commit('removeUser', id)
            }catch(err){
                console.log(err)
            }
        }
    }
}