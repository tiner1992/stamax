import VueRouter from 'vue-router'
import User from './components/pages/User.vue'
import UsersList from './components/pages/UsersList.vue'
import Main from './components/pages/Main.vue'
import NotFound from './components/pages/404.vue'
const routes = [
    {
        path: '/',
        component : Main
    },
    {
        path: '/users',
        component : UsersList,
        props: (route) => ({ page: !isNaN(Number(route.query.page)) ? Number(route.query.page) : 1 })
    },
    {
        path: '/user/:id',
        component : User,
        props: true
    },
    {
        path: '*',
        component : NotFound
    }
]

const router = new VueRouter({
    routes
})

export default router
